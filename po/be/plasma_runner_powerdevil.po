# Zmicier <zmicerturok@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: 86ff119b1606fcaa910d6b44fc14b611\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2023-02-08 13:49\n"
"Last-Translator: Zmicier <zmicerturok@gmail.com>\n"
"Language-Team: Belarusian\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || n%10>=5 && n%10<=9 || n"
"%100>=11 && n%100<=14 ? 2 : 3);\n"
"X-Generator: Lokalize 22.08.3\n"
"X-Crowdin-Project: 86ff119b1606fcaa910d6b44fc14b611\n"
"X-Crowdin-Project-ID: 127\n"
"X-Crowdin-Language: be\n"
"X-Crowdin-File: /main/be/plasma-workspace/plasma_runner_powerdevil.po\n"
"X-Crowdin-File-ID: 8874\n"

#: PowerDevilRunner.cpp:31
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "suspend"
msgstr "suspend"

#: PowerDevilRunner.cpp:33
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to ram"
msgstr "to ram"

#: PowerDevilRunner.cpp:35
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "sleep"
msgstr "sleep"

#: PowerDevilRunner.cpp:37
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hibernate"
msgstr "hibernate"

#: PowerDevilRunner.cpp:39
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to disk"
msgstr "to disk"

#: PowerDevilRunner.cpp:41 PowerDevilRunner.cpp:43 PowerDevilRunner.cpp:64
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "dim screen"
msgstr "dim screen"

#: PowerDevilRunner.cpp:52
#, kde-format
msgid ""
"Lists system suspend (e.g. sleep, hibernate) options and allows them to be "
"activated"
msgstr ""
"Вывад параметраў прыпынення сістэмы (прыпыненне, рэжым сну) і магчымасць іх "
"актывацыі"

#: PowerDevilRunner.cpp:56
#, kde-format
msgid "Suspends the system to RAM"
msgstr "Прыпыненне з кэшаваннем у аператыўную памяць"

#: PowerDevilRunner.cpp:60
#, kde-format
msgid "Suspends the system to disk"
msgstr "Прыпыненне з кэшаваннем на цвёрды дыск"

#: PowerDevilRunner.cpp:63
#, kde-format
msgctxt ""
"Note this is a KRunner keyword, <> is a placeholder and should be at the end"
msgid "screen brightness <percent value>"
msgstr "screen brightness <percent value>"

#: PowerDevilRunner.cpp:66
#, no-c-format, kde-format
msgid ""
"Lists screen brightness options or sets it to the brightness defined by the "
"search term; e.g. screen brightness 50 would dim the screen to 50% maximum "
"brightness"
msgstr ""
"Вывад параметраў яркасці экрана або вызначэнне яркасці; напрыклад, пры "
"\"screen brightness 50\" экран зацямніцца да 50% максімальнай яркасці"

#: PowerDevilRunner.cpp:88
#, kde-format
msgid "Set Brightness to %1%"
msgstr "Вызначыць яркасць %1%"

#: PowerDevilRunner.cpp:97
#, kde-format
msgid "Dim screen totally"
msgstr "Поўнае зацямненне экрана"

#: PowerDevilRunner.cpp:105
#, kde-format
msgid "Dim screen by half"
msgstr "Зацямненне экрана напалову"

#: PowerDevilRunner.cpp:135
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Прыпыніць у RAM"

#: PowerDevilRunner.cpp:136
#, kde-format
msgid "Suspend to RAM"
msgstr "Прыпыніць з кэшаваннем у аператыўную памяць"

#: PowerDevilRunner.cpp:141
#, kde-format
msgctxt "Suspend to disk"
msgid "Hibernate"
msgstr "Перавесці ў рэжым сну"

#: PowerDevilRunner.cpp:142
#, kde-format
msgid "Suspend to disk"
msgstr "Прыпыніць з кэшаваннем на цвёрды дыск"

#: PowerDevilRunner.cpp:210
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "screen brightness "
msgstr "screen brightness "

#: PowerDevilRunner.cpp:212
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "dim screen "
msgstr "dim screen "
