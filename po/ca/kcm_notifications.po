# Translation of kcm_notifications.po to Catalan
# Copyright (C) 2019-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2019, 2020, 2021, 2022, 2023.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019, 2020.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-29 01:59+0000\n"
"PO-Revision-Date: 2023-07-29 08:07+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.3\n"

#: kcm.cpp:71
#, kde-format
msgid "Toggle do not disturb"
msgstr "Commuta a no destorbar"

#: sourcesmodel.cpp:391
#, kde-format
msgid "Other Applications"
msgstr "Altres aplicacions"

#: ui/ApplicationConfiguration.qml:64
#, kde-format
msgid "Show popups"
msgstr "Mostra els missatges emergents"

#: ui/ApplicationConfiguration.qml:78
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Mostra en el mode no destorbar"

#: ui/ApplicationConfiguration.qml:91 ui/main.qml:147
#, kde-format
msgid "Show in history"
msgstr "Mostra a l'historial"

#: ui/ApplicationConfiguration.qml:102
#, kde-format
msgid "Show notification badges"
msgstr "Mostra els distintius de notificació"

#: ui/ApplicationConfiguration.qml:123
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis."
msgstr ""
"Aquesta aplicació no permet la configuració de notificacions en una base per "
"esdeveniment."

#: ui/ApplicationConfiguration.qml:131
#, kde-format
msgid "Configure events:"
msgstr "Configura els esdeveniments:"

#: ui/ApplicationConfiguration.qml:227
#, kde-format
msgid "Show a message in a pop-up"
msgstr "Mostra un missatge en una finestra emergent"

#: ui/ApplicationConfiguration.qml:236
#, kde-format
msgid "Play a sound"
msgstr "Reprodueix un so"

#: ui/main.qml:41
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"No s'ha pogut trobar cap giny «Notificacions» que es requereix per a mostrar "
"les notificacions. Assegureu-vos que s'ha activat a la Safata del sistema o "
"com a giny autònom."

#: ui/main.qml:52
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr ""
"Actualment les notificacions són proporcionades per «%1 %2» en lloc del "
"Plasma."

#: ui/main.qml:56
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "Actualment les notificacions no són proporcionades pel Plasma."

#: ui/main.qml:63
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "Mode no destorbar"

#: ui/main.qml:68
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "Enable:"
msgstr "Actiu:"

#: ui/main.qml:69
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "Quan les pantalles s'emmirallen"

#: ui/main.qml:81
#, kde-format
msgctxt "Enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "En compartir la pantalla"

#: ui/main.qml:96
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Keyboard shortcut:"
msgstr "Drecera del teclat:"

#: ui/main.qml:103
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "Condicions de visibilitat"

#: ui/main.qml:108
#, kde-format
msgid "Critical notifications:"
msgstr "Notificacions crítiques:"

#: ui/main.qml:109
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "Mostra en el mode no destorbar"

#: ui/main.qml:121
#, kde-format
msgid "Normal notifications:"
msgstr "Notificacions normals:"

#: ui/main.qml:122
#, kde-format
msgid "Show over full screen windows"
msgstr "Mostra sobre les finestres a pantalla completa"

#: ui/main.qml:134
#, kde-format
msgid "Low priority notifications:"
msgstr "Notificacions de prioritat baixa:"

#: ui/main.qml:135
#, kde-format
msgid "Show popup"
msgstr "Mostra una finestra emergent"

#: ui/main.qml:164
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Missatges emergents"

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Ubicació:"

#: ui/main.qml:171
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Prop de la icona de la notificació"

#: ui/main.qml:208
#, kde-format
msgid "Choose Custom Position…"
msgstr "Trieu una posició personalitzada…"

#: ui/main.qml:217 ui/main.qml:233
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 segon"
msgstr[1] "%1 segons"

#: ui/main.qml:222
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Oculta després de:"

#: ui/main.qml:245
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "Confirmació addicional"

#: ui/main.qml:250
#, kde-format
msgid "Application progress:"
msgstr "Progrés de l'aplicació:"

#: ui/main.qml:251 ui/main.qml:291
#, kde-format
msgid "Show in task manager"
msgstr "Mostra al gestor de tasques"

#: ui/main.qml:263
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Mostra a les notificacions"

#: ui/main.qml:277
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "Mantén el missatge emergent obert durant el progrés"

#: ui/main.qml:290
#, kde-format
msgid "Notification badges:"
msgstr "Distintius de notificació:"

#: ui/main.qml:302
#, kde-format
msgctxt "@title:group"
msgid "Application-specific settings"
msgstr "Configuració específica d'aplicació"

#: ui/main.qml:307
#, kde-format
msgid "Configure…"
msgstr "Configura…"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Posició del missatge emergent"

#: ui/SourcesPage.qml:18
#, kde-format
msgid "Application Settings"
msgstr "Configuració de l'aplicació"

#: ui/SourcesPage.qml:92
#, kde-format
msgid "Applications"
msgstr "Aplicacions"

#: ui/SourcesPage.qml:93
#, kde-format
msgid "System Services"
msgstr "Serveis del sistema"

#: ui/SourcesPage.qml:131
#, kde-format
msgid "No application or event matches your search term."
msgstr "Cap aplicació ni esdeveniment coincideix amb el terme de cerca."

#: ui/SourcesPage.qml:151
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior."
msgstr ""
"Seleccioneu una aplicació de la llista per a configurar el seu arranjament "
"de notificacions i el comportament."
