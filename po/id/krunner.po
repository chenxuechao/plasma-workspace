# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Wantoyo <wantoyek@gmail.com>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-02 02:09+0000\n"
"PO-Revision-Date: 2019-06-16 07:43+0700\n"
"Last-Translator: Wantoyo <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Wantoyo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wantoyek@gmail.com"

#: main.cpp:77 view.cpp:47
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:77
#, kde-format
msgid "Run Command interface"
msgstr "Antarmuka Run Command"

#: main.cpp:83
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Gunakan konten papan klip sebagai query untuk KRunner"

#: main.cpp:84
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "Mulaikan KRunner di latarbelakang, jangan menampilkannya."

#: main.cpp:85
#, kde-format
msgid "Replace an existing instance"
msgstr "Ganti sebuah instansi yang ada"

#: main.cpp:86
#, kde-format
msgid "Show only results from the given plugin"
msgstr ""

#: main.cpp:87
#, kde-format
msgid "List available plugins"
msgstr ""

#: main.cpp:94
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "Query yang dijalankan, hanya digunakan jika -c tidak disediakan"

#: main.cpp:103
#, kde-format
msgctxt "Header for command line output"
msgid "Available KRunner plugins, pluginId"
msgstr ""

#: qml/RunCommand.qml:92
#, kde-format
msgid "Configure"
msgstr ""

#: qml/RunCommand.qml:93
#, kde-format
msgid "Configure KRunner Behavior"
msgstr ""

#: qml/RunCommand.qml:96
#, kde-format
msgid "Configure KRunner…"
msgstr ""

#: qml/RunCommand.qml:109
#, kde-format
msgid "Showing only results from %1"
msgstr ""

#: qml/RunCommand.qml:123
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr ""

#: qml/RunCommand.qml:124
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr ""

#: qml/RunCommand.qml:295 qml/RunCommand.qml:296 qml/RunCommand.qml:298
#, kde-format
msgid "Show Usage Help"
msgstr ""

#: qml/RunCommand.qml:306
#, kde-format
msgid "Pin"
msgstr ""

#: qml/RunCommand.qml:307
#, kde-format
msgid "Pin Search"
msgstr ""

#: qml/RunCommand.qml:309
#, kde-format
msgid "Keep Open"
msgstr ""

#: qml/RunCommand.qml:381 qml/RunCommand.qml:386
#, kde-format
msgid "Recent Queries"
msgstr ""

#: qml/RunCommand.qml:384
#, kde-format
msgid "Remove"
msgstr ""

#~ msgid "krunner"
#~ msgstr "krunner"

#~ msgid "Show KRunner"
#~ msgstr "Tampilkan KRunner"

#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "KRunner"
#~ msgstr "KRunner"

#~ msgid "Run Command on clipboard contents"
#~ msgstr "Jalankan Perintah pada konten clipboard"

#~ msgid "Run Command"
#~ msgstr "Jalankan Perintah"

#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "Run Command"
#~ msgstr "Run Command"
