# Translation of plasma_runner_baloosearch5.po to Ukrainian
# Copyright (C) 2018 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2014, 2016, 2018, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_baloosearch5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2023-02-09 08:49+0200\n"
"Last-Translator: Fracture dept <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "Відкрити теку, яка містить дані"

#: baloosearchrunner.cpp:82
#, kde-format
msgid "Audios"
msgstr "Звук"

#: baloosearchrunner.cpp:83
#, kde-format
msgid "Images"
msgstr "Зображення"

#: baloosearchrunner.cpp:84
#, kde-format
msgid "Videos"
msgstr "Відео"

#: baloosearchrunner.cpp:85
#, kde-format
msgid "Spreadsheets"
msgstr "Електронні таблиці"

#: baloosearchrunner.cpp:86
#, kde-format
msgid "Presentations"
msgstr "Презентації"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Folders"
msgstr "Теки"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Documents"
msgstr "Документи"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Archives"
msgstr "Архіви"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Texts"
msgstr "Тексти"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Files"
msgstr "Файли"

#~ msgid "Search through files, emails and contacts"
#~ msgstr ""
#~ "Пошук у файлах, повідомленнях електронної пошти та записах контактів"

#~ msgid "Email"
#~ msgstr "Пошта"
