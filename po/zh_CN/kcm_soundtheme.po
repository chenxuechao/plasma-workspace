msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-29 01:59+0000\n"
"PO-Revision-Date: 2023-07-30 07:07\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-workspace/kcm_soundtheme.pot\n"
"X-Crowdin-File-ID: 44549\n"

#: ui/main.qml:59
#, kde-format
msgctxt ""
"%2 is a theme name or a list of theme names that the current theme inherits "
"from"
msgid "Based on: %2"
msgid_plural "Based on: %2"
msgstr[0] "基于：%2"

#: ui/main.qml:70
#, kde-format
msgctxt ""
"@label Precedes a list of buttons which can be clicked to preview the "
"theme's sounds. Keep it short"
msgid "Preview sounds:"
msgstr "试听声音："

#: ui/main.qml:82
#, kde-format
msgctxt "@info:tooltip"
msgid "Preview sound \"%1\""
msgstr "试听声音“%1”"

#: ui/main.qml:102
#, kde-format
msgctxt "@info:tooltip"
msgid "Preview the demo sound for the theme \"%1\""
msgstr "试听主题“%1”的演示声音"
